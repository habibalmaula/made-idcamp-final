package com.habibalmaula.cpfavoriteapp

import android.database.Cursor
import com.habibalmaula.cpfavoriteapp.entity.Movie
import java.util.*

object MappingHelper {

    fun mapCursorToArrayList(movieCursor: Cursor): ArrayList<Movie> {
        val movieList = ArrayList<Movie>()

        while (movieCursor.moveToNext()) {
            val id = movieCursor.getInt(movieCursor.getColumnIndexOrThrow("id"))
            val originalLanguage = movieCursor.getString(movieCursor.getColumnIndexOrThrow("original_language"))
            val originalTitle = movieCursor.getString(movieCursor.getColumnIndexOrThrow("original_title"))
            val overview = movieCursor.getString(movieCursor.getColumnIndexOrThrow("overview"))
            val popularity = movieCursor.getDouble(movieCursor.getColumnIndexOrThrow("popularity"))
            val posterPath = movieCursor.getString(movieCursor.getColumnIndexOrThrow("poster_path"))
            val releaseDate = movieCursor.getString(movieCursor.getColumnIndexOrThrow("release_date"))



            val title = movieCursor.getString(movieCursor.getColumnIndexOrThrow("title"))
            val video = movieCursor.getInt(movieCursor.getColumnIndexOrThrow("video")) > 0
            val voteAverage = movieCursor.getDouble(movieCursor.getColumnIndexOrThrow("vote_average"))
            val voteCount = movieCursor.getInt(movieCursor.getColumnIndexOrThrow("vote_count"))
            val adult = movieCursor.getInt(movieCursor.getColumnIndexOrThrow("adult")) > 0
            val backdropPath = movieCursor.getString(movieCursor.getColumnIndexOrThrow("backdrop_path"))
            val isFavorite = movieCursor.getInt(movieCursor.getColumnIndexOrThrow("isFavorite")) > 0

            movieList.add(Movie(id, originalLanguage,originalTitle, overview, popularity,posterPath,
                releaseDate, title, video, voteAverage, voteCount, adult, backdropPath, isFavorite))

        }

        return movieList
    }

    fun mapCursorToObject(movieCursor: Cursor): Movie {
        movieCursor.moveToNext()
        val id = movieCursor.getInt(movieCursor.getColumnIndexOrThrow("id"))
        val originalLanguage = movieCursor.getString(movieCursor.getColumnIndexOrThrow("original_language"))
        val originalTitle = movieCursor.getString(movieCursor.getColumnIndexOrThrow("original_title"))
        val overview = movieCursor.getString(movieCursor.getColumnIndexOrThrow("overview"))
        val popularity = movieCursor.getDouble(movieCursor.getColumnIndexOrThrow("popularity"))
        val posterPath = movieCursor.getString(movieCursor.getColumnIndexOrThrow("poster_path"))
        val releaseDate = movieCursor.getString(movieCursor.getColumnIndexOrThrow("release_date"))



        val title = movieCursor.getString(movieCursor.getColumnIndexOrThrow("title"))
        val video = movieCursor.getInt(movieCursor.getColumnIndexOrThrow("video")) > 0
        val voteAverage = movieCursor.getDouble(movieCursor.getColumnIndexOrThrow("vote_average"))
        val voteCount = movieCursor.getInt(movieCursor.getColumnIndexOrThrow("vote_count"))
        val adult = movieCursor.getInt(movieCursor.getColumnIndexOrThrow("adult")) > 0
        val backdropPath = movieCursor.getString(movieCursor.getColumnIndexOrThrow("backdrop_path"))
        val isFavorite = movieCursor.getInt(movieCursor.getColumnIndexOrThrow("isFavorite")) > 0


        return Movie(id, originalLanguage,originalTitle, overview, popularity,posterPath,
            releaseDate, title, video, voteAverage, voteCount, adult, backdropPath, isFavorite)

    }
}
