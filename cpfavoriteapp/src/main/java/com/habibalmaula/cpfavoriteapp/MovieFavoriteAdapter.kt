package com.habibalmaula.cpfavoriteapp

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.habibalmaula.cpfavoriteapp.entity.Movie
import kotlinx.android.synthetic.main.layout_item.view.*
import java.util.*

class MovieFavoriteAdapter : RecyclerView.Adapter<MovieFavoriteAdapter.MovieFavoriteViewHolder>() {
    val POSTER_BASE_URL = "https://image.tmdb.org/t/p/w185"

    var listMovies = ArrayList<Movie>()
        set(listMovies) {
            this.listMovies.clear()
            this.listMovies.addAll(listMovies)
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MovieFavoriteViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_item, parent, false)
        return MovieFavoriteViewHolder(view)
    }

    override fun getItemCount(): Int {
        return this.listMovies.size
    }

    override fun onBindViewHolder(holder: MovieFavoriteViewHolder, position: Int) {
        holder.bind(listMovies[position])
    }


    inner class MovieFavoriteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(movie: Movie) {
            with(itemView) {
                tv_title.text = movie.title
                tv_desc.text = movie.overview
                Glide.with(context)
                    .load(POSTER_BASE_URL + movie.poster_path)
                    .into(iv_poster)

                Log.d("GLIDE", movie.backdrop_path)
            }
        }
    }
}