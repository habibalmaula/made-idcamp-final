package com.habibalmaula.cpfavoriteapp

import android.annotation.SuppressLint
import android.database.ContentObserver
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.os.PersistableBundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.habibalmaula.cpfavoriteapp.entity.Movie
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private lateinit var adapter: MovieFavoriteAdapter

    companion object {
        private const val DB_TABLE = "movie_table"
        private const val AUTHORITY = "com.habibalmaula.submissionmadeidcamp"
        val URI_MOVIE: Uri = Uri.parse("content://$AUTHORITY/$DB_TABLE")

        private const val EXTRA_STATE = "EXTRA_STATE"



    }


    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = MovieFavoriteAdapter()
        rv_content_pr.setHasFixedSize(true)
        rv_content_pr.layoutManager = LinearLayoutManager(this)
        rv_content_pr.adapter = adapter


        val handlerThread = HandlerThread("DataObserver")
        handlerThread.start()
        val handler = Handler(handlerThread.looper)

        val myObserver = object : ContentObserver(handler) {
            override fun onChange(self: Boolean) {
                loadMovie()
            }
        }

        contentResolver.registerContentObserver(URI_MOVIE, true, myObserver)

        if (savedInstanceState == null){
            loadMovie()
        }else{
            val list = savedInstanceState.getParcelableArrayList<Movie>(EXTRA_STATE)
            if (list !=null){
                adapter.listMovies = list
            }
        }



    }


    private fun loadMovie(){
        GlobalScope.launch(Dispatchers.Main){
            val deferredMovies = async(Dispatchers.IO){
                val cursor = contentResolver?.query(URI_MOVIE,null, null,null,null) as Cursor

                Log.d("INI ADALAH CURSOR", cursor.toString())
                MappingHelper.mapCursorToArrayList(cursor)


            }

            val movies = deferredMovies.await()
            if (movies.size > 0){
                adapter.listMovies = movies
            }else{
                adapter.listMovies = ArrayList()
            }

        }
    }


    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)

        outState.putParcelableArrayList(EXTRA_STATE,adapter.listMovies)
    }




}
