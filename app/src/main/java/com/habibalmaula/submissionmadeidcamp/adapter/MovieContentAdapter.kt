package com.habibalmaula.submissionmadeidcamp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.habibalmaula.submissionmadeidcamp.R
import com.habibalmaula.submissionmadeidcamp.data.entity.Movie
import com.habibalmaula.submissionmadeidcamp.data.network.UtilsApi
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_content.*
import kotlinx.android.synthetic.main.item_content.view.*

class MovieContentAdapter (
    private val movieList: List<Movie>,
    private val listener:(Movie)-> Unit
):RecyclerView.Adapter<MovieContentAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_content, parent, false))

    }

    override fun getItemCount(): Int = movieList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(movieList[position], listener)
    }

    class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer{

        fun bind(movieList : Movie, listener:(Movie)-> Unit){
            Glide.with(containerView.context)
                .load(UtilsApi.BASE_POSTER_URL + movieList.poster_path)
                .placeholder(R.drawable.loading_animation)
                .error(R.drawable.ic_error_black_24dp)
                .into(containerView.iv_poster)


            tv_title.text = movieList.title
            ratingbar.rating = (movieList.vote_average/2).toFloat()
            btn_detail.setOnClickListener {
                listener(movieList)
            }

        }
    }


}