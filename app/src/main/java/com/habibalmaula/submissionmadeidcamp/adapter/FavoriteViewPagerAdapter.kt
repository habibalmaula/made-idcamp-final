package com.habibalmaula.submissionmadeidcamp.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class FavoriteViewPagerAdapter(supportFragmentManager: FragmentManager) :FragmentStatePagerAdapter(supportFragmentManager,
    BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
){
    private var inputFragment = ArrayList<Fragment>()
    private var inputTitle  = ArrayList<String>()



    override fun getPageTitle(position: Int): CharSequence? = inputTitle[position]

    override fun getItem(position: Int): Fragment = inputFragment[position]
    override fun getCount(): Int = inputFragment.size

    fun populateFragment(fragment: Fragment, title: String){
        inputFragment.add(fragment)
        inputTitle.add(title)
    }

}