package com.habibalmaula.submissionmadeidcamp.data.db.dao

import android.database.Cursor
import androidx.room.*
import com.habibalmaula.submissionmadeidcamp.data.entity.Movie

@Dao
interface MovieDao{
    @Query("SELECT * FROM movie_table")
    suspend fun getMovieList(): List<Movie>

    @Query("SELECT * FROM movie_table")
    fun getAllMovies(): Cursor

    @Query("SELECT * FROM movie_table where id = :uid")
    fun selectById(uid: Long): Cursor

    @Query("SELECT * FROM movie_table")
    fun getMovieFavorite(): List<Movie>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFavoriteMovie(movies : Movie)

    @Delete
    suspend fun deleteMovies(movies: Movie)

    @Query("SELECT EXISTS (SELECT 1 FROM movie_table WHERE id =:id)")
    suspend fun isFavorite(id: Int): Int


}