package com.habibalmaula.submissionmadeidcamp.data.network

import com.habibalmaula.submissionmadeidcamp.BuildConfig

class UtilsApi {
    companion object{
        const val BASE_URL = BuildConfig.BASE_API_URL
        const val BASE_POSTER_URL = BuildConfig.BASE_POSTER_URL
        const val API_KEY = BuildConfig.APIKEY
        const val LANGUAGE = "en-US"
    }

    private val service = RetrofitClient.getClient(BASE_URL)?.create(BaseApiService::class.java)

    suspend fun getDataMovie()= service?.getListMovie(API_KEY, LANGUAGE)?.results
    suspend fun getDataTV()= service?.getListTV(API_KEY, LANGUAGE)?.results
    suspend fun getSearchMovie(query : String)= service?.searchMovie(API_KEY, LANGUAGE, query)?.results
    suspend fun getSearchTV(query : String)= service?.searchTV(API_KEY, LANGUAGE, query)?.results

    suspend fun getReleaseToday(today : String) = service?.geReleaseTodayMovie(API_KEY, today, today)?.results

}