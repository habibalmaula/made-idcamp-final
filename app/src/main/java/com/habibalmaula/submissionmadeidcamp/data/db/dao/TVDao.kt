package com.habibalmaula.submissionmadeidcamp.data.db.dao

import androidx.room.*
import com.habibalmaula.submissionmadeidcamp.data.entity.TV

@Dao
interface TVDao {

    @Query("SELECT * FROM tv_table")
    suspend fun getTVList(): List<TV>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFavoriteTV(tv : TV)

    @Delete
    suspend fun deleteTV(tv: TV)

    @Query("SELECT EXISTS (SELECT 1 FROM tv_table WHERE id =:id)")
    suspend fun isFavorite(id: Int): Int
}