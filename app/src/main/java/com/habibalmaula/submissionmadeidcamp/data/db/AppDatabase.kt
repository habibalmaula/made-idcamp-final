package com.habibalmaula.submissionmadeidcamp.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.habibalmaula.submissionmadeidcamp.data.db.dao.MovieDao
import com.habibalmaula.submissionmadeidcamp.data.db.dao.TVDao
import com.habibalmaula.submissionmadeidcamp.data.entity.Movie
import com.habibalmaula.submissionmadeidcamp.data.entity.TV

@Database(entities = [Movie::class, TV::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase(){
    abstract fun movieDao(): MovieDao
    abstract fun tvDao(): TVDao


    companion object{
        private const val DATABASE_NAME = "movie-db"


        @Volatile private var instance: AppDatabase? = null

        operator fun invoke(context: Context) = instance ?: synchronized(this){
            instance ?: buildDatabase(context).also {
                instance = it }

        }

        fun buildDatabase(context: Context) = Room.databaseBuilder(
            context.applicationContext, AppDatabase::class.java, DATABASE_NAME
        ).build()
    }
}