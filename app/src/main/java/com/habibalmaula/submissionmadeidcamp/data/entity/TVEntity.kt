package com.habibalmaula.submissionmadeidcamp.data.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

data class TVEntity(
    val page: Int,
    val results: List<TV>,
    val total_pages: Int,
    val total_results: Int
)

@Parcelize
@Entity(tableName = "tv_table")
data class TV(

   // val genre_ids: List<Int>,
    @PrimaryKey
    val id: Int,
    val name: String,
  //  val origin_country: List<String>,
    val original_language: String,
    val original_name: String,
    val overview: String,
    val popularity: Double,
    val poster_path: String,
    val vote_average: Double,
    val vote_count: Int,
    val backdrop_path: String,
    val first_air_date: String,
    var isFavorite:Boolean = false

):Parcelable