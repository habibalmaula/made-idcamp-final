package com.habibalmaula.submissionmadeidcamp.data.network

import com.habibalmaula.submissionmadeidcamp.data.entity.MovieEntity
import com.habibalmaula.submissionmadeidcamp.data.entity.TVEntity
import retrofit2.http.GET
import retrofit2.http.Query

interface BaseApiService{
     @GET("discover/movie")
     suspend fun getListMovie(
         @Query("api_key") api_key:String,
         @Query("language")language:String
     ) : MovieEntity

    @GET("discover/tv")
    suspend fun getListTV(
        @Query("api_key") api_key:String,
        @Query("language")language:String
    ): TVEntity

    @GET("search/movie")
    suspend fun  searchMovie(
        @Query("api_key") api_key:String,
        @Query("language")language:String,
        @Query("query")query:String
    ) : MovieEntity

    @GET("search/tv")
    suspend fun  searchTV(
        @Query("api_key") api_key:String,
        @Query("language")language:String,
        @Query("query")query:String
    ) : TVEntity


    @GET("discover/movie")
    suspend fun geReleaseTodayMovie(
        @Query("api_key") api_key:String,
        @Query("primary_release_date.gte")primary_release_date_gte :String,
        @Query("primary_release_date.lte")primary_release_date_lte :String

    ) : MovieEntity



}