package com.habibalmaula.submissionmadeidcamp.utils
sealed class ProgressLoading {
    object LOADING : ProgressLoading()
    object ERROR : ProgressLoading()
    object DONE : ProgressLoading()
}