package com.habibalmaula.submissionmadeidcamp.provider

import android.content.ContentProvider
import android.content.ContentValues
import android.content.Context
import android.content.UriMatcher
import android.database.Cursor
import android.net.Uri
import com.habibalmaula.submissionmadeidcamp.data.db.AppDatabase

class FavoriteMovieProvider : ContentProvider() {

    companion object{
        private const val MOVIE_TABLE = "movie_table"
        private const val AUTHORITY ="com.habibalmaula.submissionmadeidcamp"
        private const val MOVIE = 1
        private val sUriMatcher = UriMatcher(UriMatcher.NO_MATCH)



        init {
            sUriMatcher.addURI(AUTHORITY, MOVIE_TABLE, MOVIE)
        }


    }


    override fun onCreate(): Boolean {
        return true
    }

    override fun query(
        uri: Uri,
        projection: Array<out String>?,
        selection: String?,
        selectionArgs: Array<out String>?,
        sortOrder: String?
    ): Cursor? {

        return when(sUriMatcher.match(uri)){
            MOVIE -> AppDatabase.buildDatabase(context as Context).movieDao().getAllMovies()
            else -> null
        }
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        return null
    }

    override fun update(
        uri: Uri,
        values: ContentValues?,
        selection: String?,
        selectionArgs: Array<out String>?
    ): Int {

        return 0
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<out String>?): Int {
        return 0
    }

    override fun getType(uri: Uri): String? {
        return null
    }
}


