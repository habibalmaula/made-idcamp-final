package com.habibalmaula.submissionmadeidcamp.widget

import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.Bundle
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import androidx.core.os.bundleOf
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.habibalmaula.submissionmadeidcamp.BuildConfig
import com.habibalmaula.submissionmadeidcamp.R
import com.habibalmaula.submissionmadeidcamp.data.db.AppDatabase
import com.habibalmaula.submissionmadeidcamp.data.entity.Movie

internal class StackRemoteViewsFactory(private val mContext: Context) : RemoteViewsService.RemoteViewsFactory {
    private val mMovie = ArrayList<Movie>()

    override fun onCreate() {
        val identityToken = Binder.clearCallingIdentity()
        Binder.restoreCallingIdentity(identityToken)

    }

    override fun getViewAt(position: Int): RemoteViews {

        val rv = RemoteViews(mContext.packageName, R.layout.widget_item)
        try {
            val bitmap = Glide.with(mContext)
                .asBitmap()
                .load(BuildConfig.BASE_POSTER_URL + mMovie[position].poster_path)
                .apply(RequestOptions().fitCenter())
                .submit(800, 550)
                .get()

            rv.setImageViewBitmap(R.id.imageViewWidget, bitmap)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        var extras = Bundle()

        if (mMovie.isNotEmpty()){
            extras = bundleOf(
                ImageBannerWidget.EXTRA_ITEM to mMovie[position].title
            )
        }


        val fillInIntent = Intent()
        fillInIntent.putExtras(extras)
        rv.setOnClickFillInIntent(R.id.imageViewWidget, fillInIntent)
        return rv
    }


    override fun getLoadingView(): RemoteViews? = null

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onDataSetChanged() {
        try {
            val movie = AppDatabase(mContext).movieDao().getMovieFavorite()
            mMovie.clear()
            mMovie.addAll(movie)
        }catch (e : Exception) {
            e.printStackTrace()
        }
    }

    override fun hasStableIds(): Boolean = false


    override fun getCount(): Int = mMovie.size

    override fun getViewTypeCount(): Int = 1

    override fun onDestroy() {
        AppDatabase.buildDatabase(mContext).close()
    }
}
