package com.habibalmaula.submissionmadeidcamp.widget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.RemoteViews
import android.widget.Toast
import androidx.core.net.toUri
import com.habibalmaula.submissionmadeidcamp.R


/**
 * Implementation of App Widget functionality.
 */
class ImageBannerWidget : AppWidgetProvider() {
    companion object {
        private const val TOAST_ACTION = "com.habibalmaula.submissionmadeidcamp.TOAST_ACTION"
        const val EXTRA_ITEM = "com.com.habibalmaula.submissionmadeidcamp.EXTRA_ITEM"

    }

    private fun updateAppWidget(
        context: Context, appWidgetManager: AppWidgetManager,
        appWidgetId: Int
    ) {

        val intent = Intent(context, StackWidgetService::class.java)
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
        intent.data = Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME))

        val intentUpdate = Intent(context, StackWidgetService::class.java)
        intentUpdate.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
        intentUpdate.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
        intentUpdate.data = intent.toUri(Intent.URI_INTENT_SCHEME).toUri()

        val views = RemoteViews(context.packageName, R.layout.image_banner_widget)
        views.setRemoteAdapter(R.id.stack_view, intent)
        views.setEmptyView(R.id.stack_view, R.id.empty_view)

        val toastIntent = Intent(context, ImageBannerWidget::class.java)
        toastIntent.action = TOAST_ACTION
        toastIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
        toastIntent.data = intent.toUri(Intent.URI_INTENT_SCHEME).toUri()

        val toastPendingIntent = PendingIntent.getBroadcast(context, 0, toastIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        views.setPendingIntentTemplate(R.id.stack_view, toastPendingIntent)


        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views)
    }




    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    override fun onEnabled(context: Context) {
        // Enter relevant functionality for when the first widget is created
    }

    override fun onDisabled(context: Context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)
        val mgr = AppWidgetManager.getInstance(context)

        if (intent.action == TOAST_ACTION) {
            val viewTitle = intent.getStringExtra(EXTRA_ITEM)
            Toast.makeText(context, "Touched view $viewTitle", Toast.LENGTH_SHORT).show()
            val thisWidget = ComponentName(context, ImageBannerWidget::class.java)
            val appWidgetIds = mgr.getAppWidgetIds(thisWidget)
            mgr.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.stack_view)

        }
        if (intent.action == AppWidgetManager.ACTION_APPWIDGET_UPDATE){
            val thisWidget = ComponentName(context, ImageBannerWidget::class.java)
            val appWidgetIds = mgr.getAppWidgetIds(thisWidget)
            mgr.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.stack_view)
        }


    }
}

