package com.habibalmaula.submissionmadeidcamp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.habibalmaula.submissionmadeidcamp.utils.gone
import com.habibalmaula.submissionmadeidcamp.utils.visible


class MainActivity : AppCompatActivity() {
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navController: NavController


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        

        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        navController = findNavController(R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_tv, R.id.navigation_movie, R.id.navigation_account, R.id.navigation_favorite
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

         navController.addOnDestinationChangedListener { _, destination, _ ->
             when(destination.id) {
                 R.id.detailMovieFragment -> hideNavView(navView)
                 R.id.detailTVFragment -> hideNavView(navView)
                 else -> showNavView(navView)
             }

        }
    }


    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()

    }

    private fun showNavView(navView: BottomNavigationView) {
        navView.visible()
    }

    private fun hideNavView(navView: BottomNavigationView) {
        navView.gone()
    }





}
