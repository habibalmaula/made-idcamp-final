package com.habibalmaula.submissionmadeidcamp.ui.favorite.movie

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.habibalmaula.submissionmadeidcamp.data.db.AppDatabase
import com.habibalmaula.submissionmadeidcamp.data.entity.Movie
import com.habibalmaula.submissionmadeidcamp.utils.ProgressLoading
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class MovieFavoriteViewModel(context: Context) : ViewModel() {

    private val mMovie = MutableLiveData<List<Movie>>()
    private val mProgress = MutableLiveData<ProgressLoading>()

    val listMovie : LiveData<List<Movie>>
        get() = mMovie

    val progress : LiveData<ProgressLoading>
        get() = mProgress


    init {
        getFavoriteMovie(context)
    }

    private fun getFavoriteMovie(context: Context){
        mProgress.value = ProgressLoading.LOADING
        viewModelScope.launch {
            try {
                val movie = AppDatabase(context).movieDao().getMovieList()
                mMovie.value = movie
                mProgress.value = ProgressLoading.DONE
            }catch (e: Throwable){
                mProgress.value = ProgressLoading.ERROR
                e.printStackTrace()
            }
        }
    }



    fun onRefreshMovie(context: Context){
        getFavoriteMovie(context)
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }
}
