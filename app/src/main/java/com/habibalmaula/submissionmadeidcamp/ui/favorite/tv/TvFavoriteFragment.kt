package com.habibalmaula.submissionmadeidcamp.ui.favorite.tv

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.habibalmaula.submissionmadeidcamp.R
import com.habibalmaula.submissionmadeidcamp.adapter.TVContentAdapter
import com.habibalmaula.submissionmadeidcamp.data.entity.TV
import com.habibalmaula.submissionmadeidcamp.ui.favorite.MainFavoriteFragmentDirections
import com.habibalmaula.submissionmadeidcamp.utils.ProgressLoading
import com.habibalmaula.submissionmadeidcamp.utils.gone
import com.habibalmaula.submissionmadeidcamp.utils.invisible
import com.habibalmaula.submissionmadeidcamp.utils.visible
import kotlinx.android.synthetic.main.tv_favorite_fragment.*

class TvFavoriteFragment : Fragment() {

    private lateinit var viewModel: TvFavoriteViewModel
    private lateinit var viewModelFactory: TvFavoriteViewModelFactory
    private lateinit var tvContentAdapter: TVContentAdapter

    private var tvContent : MutableList<TV> = mutableListOf()



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.tv_favorite_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModelFactory = TvFavoriteViewModelFactory(requireContext())
        viewModel = ViewModelProvider(this, viewModelFactory).get(TvFavoriteViewModel::class.java)

        viewModel.listTv.observe(this, Observer {
            tvContent.clear()
            tvContent.addAll(it)
            tvContentAdapter.notifyDataSetChanged()
        })

        tvContentAdapter = TVContentAdapter(tvContent){
            this.findNavController()
                .navigate(MainFavoriteFragmentDirections.actionNavigationFavoriteToDetailTVFragment(it,it.name))

        }

        rv_tv_favorite.adapter = tvContentAdapter

        viewModel.listTv.observe(this, Observer {
            if (it.isEmpty()){
                content_no_data_tv_favorite.visible()
            }else content_no_data_tv_favorite.gone()
        })


        viewModel.progress.observe(this, Observer {
            when(it){
                ProgressLoading.LOADING ->{
                    progress_tv_favorite.visible()
                    content_error_tv_favorite.gone()
                    rv_tv_favorite.invisible()
                }

                ProgressLoading.DONE->{
                    progress_tv_favorite.gone()
                    content_error_tv_favorite.gone()
                    rv_tv_favorite.visible()
                }

                ProgressLoading.ERROR->{
                    progress_tv_favorite.gone()
                    content_error_tv_favorite.visible()
                    rv_tv_favorite.gone()
                }
            }
        })

        refresh_tv_favorite.setOnRefreshListener {
            refresh_tv_favorite.isRefreshing = false
            viewModel.onRefreshMovie(requireContext())
        }
    }

    override fun onResume() {
        super.onResume()

        viewModel.onRefreshMovie(requireContext())

    }

}
