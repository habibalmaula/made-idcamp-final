package com.habibalmaula.submissionmadeidcamp.ui.favorite.tv

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class TvFavoriteViewModelFactory (private val context: Context): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TvFavoriteViewModel::class.java)) {
            return TvFavoriteViewModel(context) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }


}