package com.habibalmaula.submissionmadeidcamp.ui.detail.tv

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.habibalmaula.submissionmadeidcamp.data.db.AppDatabase
import com.habibalmaula.submissionmadeidcamp.data.entity.TV
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class DetailTVViewModel(context: Context, id: Int, detailContent: TV) : ViewModel() {
    private val mfavbool = MutableLiveData<Boolean>()
    private val mdetailContent = MutableLiveData<TV>()

    val favbool: LiveData<Boolean>
        get() = mfavbool

    val detailContent: LiveData<TV>
        get() = mdetailContent

    init {
        isFavorite(context, id)
        loadDataArgs(detailContent)
    }


    private fun loadDataArgs(detailContent: TV): MutableLiveData<TV> {
        mdetailContent.value = detailContent

        return mdetailContent
    }

    private fun isFavorite(context: Context, id: Int): MutableLiveData<Boolean> {
        viewModelScope.launch {
            try {
                val favorite = AppDatabase(context).tvDao().isFavorite(id)
                mfavbool.value = favorite == 1
            } catch (e: Throwable) {
                e.printStackTrace()
            }
        }

        return mfavbool
    }

    fun addFavorite(context: Context, tv: TV) {
        viewModelScope.launch {
            AppDatabase(context).tvDao().insertFavoriteTV(tv)
        }
    }


    fun removeFromFavorite(context: Context, tv: TV) {
        viewModelScope.launch {
            AppDatabase(context).tvDao().deleteTV(tv)
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }


}