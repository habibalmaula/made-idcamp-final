package com.habibalmaula.submissionmadeidcamp.ui.detail.tv

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.habibalmaula.submissionmadeidcamp.data.entity.TV

class DetailTVViewModelFactory (private val context: Context, private val id : Int, private val detailContent: TV): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DetailTVViewModel::class.java)) {
            return DetailTVViewModel(context, id, detailContent) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }


}