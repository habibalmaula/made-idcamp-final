package com.habibalmaula.submissionmadeidcamp.ui.favorite


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.habibalmaula.submissionmadeidcamp.R
import com.habibalmaula.submissionmadeidcamp.adapter.FavoriteViewPagerAdapter
import com.habibalmaula.submissionmadeidcamp.ui.favorite.movie.MovieFavoriteFragment
import com.habibalmaula.submissionmadeidcamp.ui.favorite.tv.TvFavoriteFragment
import kotlinx.android.synthetic.main.fragment_main_favorite.*

/**
 * A simple [Fragment] subclass.
 */
class MainFavoriteFragment : Fragment() {

    private lateinit var favoriteViewPagerAdapter : FavoriteViewPagerAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_favorite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        favoriteViewPagerAdapter = FavoriteViewPagerAdapter(childFragmentManager).apply {
            populateFragment(MovieFavoriteFragment(), resources.getString(R.string.title_movie))
            populateFragment(TvFavoriteFragment(), resources.getString(R.string.title_TV))

        }
      viewPager.adapter = favoriteViewPagerAdapter
        tabs.setupWithViewPager(viewPager)

    }


}
