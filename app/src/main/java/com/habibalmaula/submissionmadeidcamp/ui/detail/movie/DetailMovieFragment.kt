package com.habibalmaula.submissionmadeidcamp.ui.detail.movie


import android.content.pm.ActivityInfo
import android.os.Bundle
import android.os.Handler
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.habibalmaula.submissionmadeidcamp.R
import com.habibalmaula.submissionmadeidcamp.data.entity.Movie
import com.habibalmaula.submissionmadeidcamp.data.network.UtilsApi
import com.habibalmaula.submissionmadeidcamp.utils.gone
import com.habibalmaula.submissionmadeidcamp.utils.visible
import kotlinx.android.synthetic.main.fragment_detail_movie.*


/**
 * A simple [Fragment] subclass.
 */
class DetailMovieFragment : Fragment() {
    private val movieContent by navArgs<DetailMovieFragmentArgs>()

    private lateinit var handler: Handler
    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false

    private val detailMovieViewModel : DetailMovieViewModel by lazy {
        ViewModelProvider(this, DetailMovieViewModelFactory(requireContext(),
           movieContent.movieData.id, movieContent.movieData)).get(DetailMovieViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_movie, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setHasOptionsMenu(true)
        handler = Handler()


        detailMovieViewModel.detailContent.observe(this, Observer {
            setupContent(it)
            setFavorite()


            handler.postDelayed(
                {
                    loading_detail_movie.gone()
                    main_view_detail_movie.visible()
                },1500
            )

        })
        detailMovieViewModel.favbool.observe(this, Observer {
            isFavorite = it
            setFavorite()
        })


    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.favorite_menu, menu)
        menuItem = menu
        setFavorite()
        super.onCreateOptionsMenu(menu, inflater)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.add_to_favorite ->{
                if (isFavorite){
                    AlertDialog.Builder(requireContext()).apply {
                        setTitle(resources.getString(R.string.action_remove))
                        setCancelable(false)
                        setMessage(resources.getString(R.string.confirm_delete))
                        setPositiveButton(resources.getString(R.string.yes)) { _, _ ->
                            detailMovieViewModel.removeFromFavorite(context,movieContent.movieData)
                            Snackbar.make(rootLayout, R.string.removed_from_favorite, Snackbar.LENGTH_SHORT).show()

                        }
                        setNegativeButton(resources.getString(R.string.no)) { _, _ ->
                            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_favorite_added)
                            isFavorite = true
                        }
                    }.create().show()

                } else{
                    detailMovieViewModel.addFavorite(requireContext(),movieContent.movieData)
                    Snackbar.make(rootLayout, R.string.added_to_favorite, Snackbar.LENGTH_SHORT).show()
                }

                isFavorite = !isFavorite
                setFavorite()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }


    private fun setFavorite() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_favorite_added)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_favorite_add)
    }

    private fun setupContent(movieContent: Movie) {
        Glide.with(this)
            .load(UtilsApi.BASE_POSTER_URL+movieContent.backdrop_path)
            .into(iv_backdrop_movie)

        Glide.with(this)
            .load(UtilsApi.BASE_POSTER_URL + movieContent.poster_path)
            .into(iv_poster_detail_movie)

        ratingbar_detail_movie.rating = (movieContent.vote_average/2).toFloat()
        tv_rating_movie.text= movieContent.vote_average.toString()

        tv_detail_title_movie.text = movieContent.title

        tv_release_movie.text = movieContent.release_date
        tv_description_movie.text = movieContent.overview

    }

    override fun onResume() {
        super.onResume()
        setFavorite()
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

    }

    override fun onPause() {
        super.onPause()
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR


    }


}
