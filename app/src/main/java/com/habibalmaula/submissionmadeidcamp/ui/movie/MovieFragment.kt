package com.habibalmaula.submissionmadeidcamp.ui.movie

import android.os.Bundle
import android.os.Handler
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.habibalmaula.submissionmadeidcamp.R
import com.habibalmaula.submissionmadeidcamp.adapter.MovieContentAdapter
import com.habibalmaula.submissionmadeidcamp.data.entity.Movie
import com.habibalmaula.submissionmadeidcamp.utils.ProgressLoading
import com.habibalmaula.submissionmadeidcamp.utils.gone
import com.habibalmaula.submissionmadeidcamp.utils.invisible
import com.habibalmaula.submissionmadeidcamp.utils.visible
import kotlinx.android.synthetic.main.fragment_movie.*

@Suppress("WHEN_ENUM_CAN_BE_NULL_IN_JAVA")
class MovieFragment : Fragment() {

    private val movieViewModel :MovieViewModel by lazy {
        ViewModelProvider(this).get(MovieViewModel::class.java)
    }

    private var movieContent: MutableList<Movie> = mutableListOf()
    private lateinit var movieContentAdapter: MovieContentAdapter
    private lateinit var handler: Handler



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        return inflater.inflate(R.layout.fragment_movie, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setHasOptionsMenu(true)
        handler = Handler()


        movieViewModel.getContentMovie()

        refresh_movie.setOnRefreshListener {
            refresh_movie.isRefreshing = false
            movieViewModel.onRefreshMovie()
        }

        movieViewModel.listMovie.observe(this, Observer {
            movieContent.clear()
            movieContent.addAll(it)
            movieContentAdapter.notifyDataSetChanged()

        })
        movieContentAdapter = MovieContentAdapter(movieContent){
            this.findNavController()
                .navigate(MovieFragmentDirections.actionMovieFragmentToDetailMovieFragment(
                    it,
                    it.title
                ))
        }
        rv_movie.adapter = movieContentAdapter


        movieViewModel.progress.observe(this, Observer {
            when(it){
                ProgressLoading.LOADING ->{
                    progress_movie.visible()
                    content_error_movie.gone()
                    rv_movie.invisible()
                }

                ProgressLoading.DONE->{
                    progress_movie.gone()
                    content_error_movie.gone()
                    rv_movie.visible()
                }

                ProgressLoading.ERROR->{
                    progress_movie.gone()
                    content_error_movie.visible()
                    rv_movie.gone()
                }
            }
        })

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)

        val searchView = menu.findItem(R.id.search_item).actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {

                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText.isNullOrEmpty()){
                    handler.postDelayed(
                        {
                            movieViewModel.getContentMovie()
                        },1000
                    )

                }else{
                    handler.postDelayed(
                        {
                            newText.let { movieViewModel.searchMovie(it) }
                        },1000
                    )
                }



                return true
            }


        })



        super.onCreateOptionsMenu(menu, inflater)
    }
}