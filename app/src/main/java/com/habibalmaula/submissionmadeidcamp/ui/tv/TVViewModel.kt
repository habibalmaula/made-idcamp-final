package com.habibalmaula.submissionmadeidcamp.ui.tv

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.habibalmaula.submissionmadeidcamp.data.entity.TV
import com.habibalmaula.submissionmadeidcamp.data.network.UtilsApi
import com.habibalmaula.submissionmadeidcamp.utils.ProgressLoading
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class TVViewModel : ViewModel() {

    private val utilsApi = UtilsApi()
    private val mTV = MutableLiveData<List<TV>>()
    private val mProgress = MutableLiveData<ProgressLoading>()

    val listTv : LiveData<List<TV>>
        get() = mTV

    val progress : LiveData<ProgressLoading>
        get() = mProgress


    fun getContentTV(){
        mProgress.value= ProgressLoading.LOADING
        viewModelScope.launch {
            try {
                val response = utilsApi.getDataTV()
                mTV.value = response
                mProgress.value= ProgressLoading.DONE
            }catch (e:Throwable){
                mProgress.value = ProgressLoading.ERROR
                e.printStackTrace()
            }
        }
    }

    fun searchTV(query: String){
        mProgress.value= ProgressLoading.LOADING
        viewModelScope.launch {
            try {
                val response = utilsApi.getSearchTV(query)
                mTV.value = response
                mProgress.value= ProgressLoading.DONE
            }catch (e:Throwable){
                mProgress.value = ProgressLoading.ERROR
                e.printStackTrace()
            }
        }
    }

    fun onRefreshTV(){
        getContentTV()
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }

}