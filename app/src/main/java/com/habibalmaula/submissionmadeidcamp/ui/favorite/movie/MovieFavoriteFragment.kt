package com.habibalmaula.submissionmadeidcamp.ui.favorite.movie

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.habibalmaula.submissionmadeidcamp.R
import com.habibalmaula.submissionmadeidcamp.adapter.MovieContentAdapter
import com.habibalmaula.submissionmadeidcamp.data.entity.Movie
import com.habibalmaula.submissionmadeidcamp.ui.favorite.MainFavoriteFragmentDirections
import com.habibalmaula.submissionmadeidcamp.utils.ProgressLoading
import com.habibalmaula.submissionmadeidcamp.utils.gone
import com.habibalmaula.submissionmadeidcamp.utils.invisible
import com.habibalmaula.submissionmadeidcamp.utils.visible
import kotlinx.android.synthetic.main.movie_favorite_fragment.*

class MovieFavoriteFragment : Fragment() {

    private lateinit var movieContentAdapter: MovieContentAdapter

    private var movieContent: MutableList<Movie> = mutableListOf()

    private lateinit var viewModel: MovieFavoriteViewModel
    private lateinit var viewModelFactory: MovieFavoriteViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.movie_favorite_fragment, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModelFactory = MovieFavoriteViewModelFactory(requireContext())
        viewModel = ViewModelProvider(this, viewModelFactory).get(MovieFavoriteViewModel::class.java)

        viewModel.listMovie.observe(this, Observer {
            movieContent.clear()
            movieContent.addAll(it)
            movieContentAdapter.notifyDataSetChanged()

        })

        movieContentAdapter = MovieContentAdapter(movieContent){
            this.findNavController()
                .navigate(
                    MainFavoriteFragmentDirections.actionNavigationFavoriteToDetailMovieFragment(it, it.title))

        }
        rv_movie_favorite.adapter = movieContentAdapter

        viewModel.listMovie.observe(this, Observer {
            if (it.isEmpty()){
                content_no_data_movie_favorite.visible()
            }else content_no_data_movie_favorite.gone()
        })


        viewModel.progress.observe(this, Observer {
            when(it){
                ProgressLoading.LOADING ->{
                    progress_movie_favorite.visible()
                    content_error_movie_favorite.gone()
                    rv_movie_favorite.invisible()
                }

                ProgressLoading.DONE->{
                    progress_movie_favorite.gone()
                    content_error_movie_favorite.gone()
                    rv_movie_favorite.visible()
                }

                ProgressLoading.ERROR->{
                    progress_movie_favorite.gone()
                    content_error_movie_favorite.visible()
                    rv_movie_favorite.gone()
                }
            }
        })

        refresh_movie_favorite.setOnRefreshListener {
            refresh_movie_favorite.isRefreshing = false
            viewModel.onRefreshMovie(requireContext())
        }


    }


    override fun onResume() {
        super.onResume()

        viewModel.onRefreshMovie(requireContext())

    }




}
