package com.habibalmaula.submissionmadeidcamp.ui.detail.tv


import android.content.pm.ActivityInfo
import android.os.Bundle
import android.os.Handler
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.habibalmaula.submissionmadeidcamp.R
import com.habibalmaula.submissionmadeidcamp.data.entity.TV
import com.habibalmaula.submissionmadeidcamp.data.network.UtilsApi
import com.habibalmaula.submissionmadeidcamp.utils.gone
import com.habibalmaula.submissionmadeidcamp.utils.visible
import kotlinx.android.synthetic.main.fragment_detail_tv.*

/**
 * A simple [Fragment] subclass.
 */
class DetailTVFragment : Fragment() {
    private val tvContent by navArgs<DetailTVFragmentArgs>()
    private lateinit var handler: Handler
    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false

    private val detailTVViewModel : DetailTVViewModel by lazy {
        ViewModelProvider(this, DetailTVViewModelFactory(requireContext(),
            tvContent.tvData.id, tvContent.tvData)
        ).get(DetailTVViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_tv, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)

        detailTVViewModel.detailContent.observe(this, Observer {
            setupContent(it)
        })

        detailTVViewModel.favbool.observe(this, Observer {
            isFavorite = it
            setFavorite()

        })

        handler = Handler()
        handler.postDelayed({
            loading_detail_tv.gone()
            main_view_detail_tv.visible()
        },1500)

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.add_to_favorite ->{
                if (isFavorite){
                    AlertDialog.Builder(requireContext()).apply {
                        setTitle(resources.getString(R.string.action_remove))
                        setCancelable(false)
                        setMessage(resources.getString(R.string.confirm_delete))
                        setPositiveButton(resources.getString(R.string.yes)) { _, _ ->
                            detailTVViewModel.removeFromFavorite(context,tvContent.tvData)
                            Snackbar.make(rootLayouttv, R.string.removed_from_favorite, Snackbar.LENGTH_SHORT).show()

                        }
                        setNegativeButton(resources.getString(R.string.no)) { _, _ ->
                            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_favorite_added)
                            isFavorite = true
                        }
                    }.create().show()

                } else{
                    detailTVViewModel.addFavorite(requireContext(),tvContent.tvData)
                    Snackbar.make(rootLayouttv, R.string.added_to_favorite, Snackbar.LENGTH_SHORT).show()
                }

                isFavorite = !isFavorite
                setFavorite()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }

        }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.favorite_menu, menu)
        menuItem = menu
        setFavorite()

        super.onCreateOptionsMenu(menu, inflater)

    }

    private fun setupContent(tvContent: TV) {
        Glide.with(this)
            .load(UtilsApi.BASE_POSTER_URL+tvContent.backdrop_path)
            .into(iv_backdrop_tv)

        Glide.with(this)
            .load(UtilsApi.BASE_POSTER_URL + tvContent.poster_path)
            .into(iv_poster_detail_tv)

        ratingbar_detail_tv.rating = (tvContent.vote_average/2).toFloat()
        tv_rating_tv.text= tvContent.vote_average.toString()

        tv_detail_title_tv.text = tvContent.name

        tv_release_tv.text = tvContent.first_air_date
        tv_description_tv.text = tvContent.overview

    }

    private fun setFavorite() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_favorite_added)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_favorite_add)
    }

    override fun onResume() {
        super.onResume()
        setFavorite()
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

    }

    override fun onPause() {
        super.onPause()
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR


    }


}
