package com.habibalmaula.submissionmadeidcamp.ui.detail.movie

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.habibalmaula.submissionmadeidcamp.data.entity.Movie

class DetailMovieViewModelFactory (private val context: Context, private val id : Int, private val detailContent: Movie): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DetailMovieViewModel::class.java)) {
            return DetailMovieViewModel(context, id, detailContent) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }


}