package com.habibalmaula.submissionmadeidcamp.ui.account

import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.work.*
import androidx.work.Data.Builder
import com.habibalmaula.submissionmadeidcamp.R
import com.habibalmaula.submissionmadeidcamp.reminder.ReminderWorker
import com.habibalmaula.submissionmadeidcamp.reminder.receiver.DailyReminder
import kotlinx.android.synthetic.main.fragment_account.*
import java.text.SimpleDateFormat
import java.util.*

class AccountFragment : Fragment() {

    private lateinit var accountViewModel: AccountViewModel
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var sharedPreferenceEdit: SharedPreferences.Editor
    private lateinit var dailyReminder: DailyReminder


    companion object{
        const val MY_SHARED_PREFERENCES = "MY_SHARED_PREFERENCES"
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        accountViewModel =
            ViewModelProvider(this).get(AccountViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_account, container, false)
        accountViewModel.text.observe(this, Observer {
        })
        return root
    }


    @TargetApi(Build.VERSION_CODES.O)
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        change_language.setOnClickListener {
            val mIntent = Intent(Settings.ACTION_LOCALE_SETTINGS)
            startActivity(mIntent)
        }


        btn_test_notif.setOnClickListener {
            startNotifOneTime()
        }


        sharedPreferences = requireContext().getSharedPreferences(MY_SHARED_PREFERENCES, Context.MODE_PRIVATE)
        sharedPreferenceEdit = sharedPreferences.edit()
        dailyReminder = DailyReminder()

        setSwitchChange()
        setPreferences()

    }

    private fun setSwitchChange() {
        switch_notifyGetback.setOnCheckedChangeListener { _, isChecked ->
            sharedPreferenceEdit.putBoolean("daily_reminder", isChecked)
            sharedPreferenceEdit.apply()
            if (isChecked) {
                dailyReminder.setDailyReminder(requireContext())



            } else {
                dailyReminder.cancelDailyReminder(requireContext())

            }
        }

        switch_notifyNewRelease.setOnCheckedChangeListener { _, isChecked ->
            sharedPreferenceEdit.putBoolean("release_reminder", isChecked)
            sharedPreferenceEdit.apply()
            if (isChecked) {
                dailyReminder.setReleaseTodayReminder(requireContext())

            } else {
                dailyReminder.cancelReleaseToday(requireContext())

            }
        }
    }


    private fun setPreferences() {
        val dailyReminder = sharedPreferences.getBoolean("daily_reminder", false)
        val releaseReminder = sharedPreferences.getBoolean("release_reminder", false)

        switch_notifyGetback.isChecked = dailyReminder
        switch_notifyNewRelease.isChecked = releaseReminder
    }


    @TargetApi(Build.VERSION_CODES.O)
    private fun startNotifOneTime() {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val date = Date()
        val now = dateFormat.format(date)




        val data =  Builder()
            .putString(ReminderWorker.EXTRA_DATE, now)
            .build()

        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        val oneTimeWorkRequest = OneTimeWorkRequest.Builder(ReminderWorker::class.java)
            .setInputData(data)
            .setConstraints(constraints)
            .build()



        WorkManager.getInstance(requireContext()).enqueue(oneTimeWorkRequest)

        WorkManager.getInstance(requireContext()).getWorkInfoByIdLiveData(oneTimeWorkRequest.id).observe(this,
            Observer<WorkInfo> { workInfo ->
                val status = workInfo.state.name

                println(status)

            })

    }





}