package com.habibalmaula.submissionmadeidcamp.ui.favorite.tv

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.habibalmaula.submissionmadeidcamp.data.db.AppDatabase
import com.habibalmaula.submissionmadeidcamp.data.entity.TV
import com.habibalmaula.submissionmadeidcamp.utils.ProgressLoading
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class TvFavoriteViewModel(context: Context) : ViewModel() {
    private val mTV = MutableLiveData<List<TV>>()
    private val mProgress = MutableLiveData<ProgressLoading>()

    val listTv : LiveData<List<TV>>
        get() = mTV

    val progress : LiveData<ProgressLoading>
        get() = mProgress

    init {
        getFavoriteTV(context)
    }

    private fun getFavoriteTV(context: Context){
        mProgress.value = ProgressLoading.LOADING
        viewModelScope.launch {
            try {
                val tv = AppDatabase(context).tvDao().getTVList()
                mTV.value = tv
                mProgress.value = ProgressLoading.DONE
            }catch (e: Throwable){
                mProgress.value = ProgressLoading.ERROR
                e.printStackTrace()
            }
        }
    }



    fun onRefreshMovie(context: Context){
        getFavoriteTV(context)
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }
}
