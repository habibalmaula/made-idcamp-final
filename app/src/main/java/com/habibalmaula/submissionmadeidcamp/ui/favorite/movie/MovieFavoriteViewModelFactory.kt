package com.habibalmaula.submissionmadeidcamp.ui.favorite.movie

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MovieFavoriteViewModelFactory (private val context: Context): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MovieFavoriteViewModel::class.java)) {
            return MovieFavoriteViewModel(context) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }


}