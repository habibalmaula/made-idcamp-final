package com.habibalmaula.submissionmadeidcamp.ui.detail.movie

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.habibalmaula.submissionmadeidcamp.data.db.AppDatabase
import com.habibalmaula.submissionmadeidcamp.data.entity.Movie
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class DetailMovieViewModel(context: Context, id: Int, detailContent: Movie) : ViewModel(){
    private val mfavbool = MutableLiveData<Boolean>()
    private val mdetailContent = MutableLiveData<Movie>()

    val favbool : LiveData<Boolean>
        get() = mfavbool

    val detailContent : LiveData<Movie>
        get() = mdetailContent

    init {
        isFavorite(context, id)
        loadDataArgs(detailContent)
    }


    private fun loadDataArgs(detailContent: Movie): MutableLiveData<Movie>{
        mdetailContent.value = detailContent

        return mdetailContent
    }
    private fun isFavorite(context: Context, id:Int) : MutableLiveData<Boolean>{
        viewModelScope.launch {
            try {
                val favorite = AppDatabase(context).movieDao().isFavorite(id)
                mfavbool.value = favorite ==1
            }catch (e: Throwable){
                e.printStackTrace()
            }
        }

        return mfavbool
    }

    fun addFavorite(context: Context, movie: Movie){
        viewModelScope.launch {
            AppDatabase(context).movieDao().insertFavoriteMovie(movie)
        }
    }


    fun removeFromFavorite(context: Context,movie: Movie){
        viewModelScope.launch {
            AppDatabase(context).movieDao().deleteMovies(movie)
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }


}