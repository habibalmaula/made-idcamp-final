package com.habibalmaula.submissionmadeidcamp.ui.movie

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.habibalmaula.submissionmadeidcamp.data.entity.Movie
import com.habibalmaula.submissionmadeidcamp.data.network.UtilsApi
import com.habibalmaula.submissionmadeidcamp.utils.ProgressLoading
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class MovieViewModel : ViewModel() {

    private val utilsApi = UtilsApi()
    private val mMovie = MutableLiveData<List<Movie>>()
    private val mProgress = MutableLiveData<ProgressLoading>()

    val listMovie : LiveData<List<Movie>>
        get() = mMovie

    val progress : LiveData<ProgressLoading>
        get() = mProgress



    fun getContentMovie(){
        mProgress.value = ProgressLoading.LOADING
        viewModelScope.launch {
            try {
                val response = utilsApi.getDataMovie()
                mMovie.value = response
                mProgress.value = ProgressLoading.DONE
            }catch (e: Throwable){
                mProgress.value = ProgressLoading.ERROR
                e.printStackTrace()
            }
        }
    }

    fun searchMovie(query : String){
        mProgress.value = ProgressLoading.LOADING
        viewModelScope.launch {
            try {
                val response = utilsApi.getSearchMovie(query)
                mMovie.value = response
                mProgress.value = ProgressLoading.DONE
            }catch (e: Throwable){
                mProgress.value = ProgressLoading.ERROR
                e.printStackTrace()
            }
        }
    }



     fun onRefreshMovie(){
        getContentMovie()
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }
}