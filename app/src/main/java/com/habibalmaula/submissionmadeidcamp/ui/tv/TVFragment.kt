package com.habibalmaula.submissionmadeidcamp.ui.tv

import android.os.Bundle
import android.os.Handler
import android.view.*
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.habibalmaula.submissionmadeidcamp.R
import com.habibalmaula.submissionmadeidcamp.adapter.TVContentAdapter
import com.habibalmaula.submissionmadeidcamp.data.entity.TV
import com.habibalmaula.submissionmadeidcamp.utils.ProgressLoading
import com.habibalmaula.submissionmadeidcamp.utils.gone
import com.habibalmaula.submissionmadeidcamp.utils.invisible
import com.habibalmaula.submissionmadeidcamp.utils.visible
import kotlinx.android.synthetic.main.fragment_tv.*

class TVFragment : Fragment() {
    private val tvViewModel : TVViewModel by lazy {
        ViewModelProvider(this).get(TVViewModel::class.java)
    }

    private lateinit var handler: Handler

    private var tvContent : MutableList<TV> = mutableListOf()

    private lateinit var tvContentAdapter: TVContentAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_tv, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        tvViewModel.getContentTV()

        setHasOptionsMenu(true)
        handler = Handler()


        refresh_tv.setOnRefreshListener {
            refresh_tv.isRefreshing = false
            tvViewModel.onRefreshTV()

        }

        tvViewModel.listTv.observe(this, Observer {
            tvContent.clear()
            tvContent.addAll(it)
            tvContentAdapter.notifyDataSetChanged()
        })
        val layoutManager = GridLayoutManager(requireContext(),2)

        tvContentAdapter = TVContentAdapter(tvContent){
            this.findNavController()
                .navigate(TVFragmentDirections.actionNavigationHomeToDetailTVFragment(
                    it,
                    it.name
                ))
        }
        rv_tv.layoutManager = layoutManager

        rv_tv.adapter = tvContentAdapter


        tvViewModel.progress.observe(this, Observer {
            when(it){
                ProgressLoading.LOADING ->{
                    progress_tv.visible()
                    content_error_tv.gone()
                    rv_tv.invisible()
                }

                ProgressLoading.DONE->{
                    progress_tv.gone()
                    content_error_tv.gone()
                    rv_tv.visible()
                }

                ProgressLoading.ERROR->{
                    progress_tv.gone()
                    content_error_tv.visible()
                    rv_tv.gone()
                }
            }
        })

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)

        val searchView = menu.findItem(R.id.search_item).actionView as SearchView

        searchView.setOnQueryTextListener(object :SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                Toast.makeText(requireContext(),query, Toast.LENGTH_SHORT).show()

                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText.isNullOrEmpty()){
                    handler.postDelayed(
                        {
                            tvViewModel.getContentTV()
                        },1000
                    )

                }else{
                    handler.postDelayed(
                        {
                            newText.let { tvViewModel.searchTV(it) }
                        },1000
                    )
                }

                return true
            }


        })



        super.onCreateOptionsMenu(menu, inflater)
    }
}