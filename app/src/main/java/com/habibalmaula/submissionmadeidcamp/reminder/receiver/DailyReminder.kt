package com.habibalmaula.submissionmadeidcamp.reminder.receiver

import android.annotation.TargetApi
import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import com.habibalmaula.submissionmadeidcamp.R
import com.habibalmaula.submissionmadeidcamp.data.entity.Movie
import com.habibalmaula.submissionmadeidcamp.data.network.UtilsApi
import com.habibalmaula.submissionmadeidcamp.ui.account.AccountFragment
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class DailyReminder : BroadcastReceiver() {

    private val utilsApi = UtilsApi()
    private val mMovie = ArrayList<Movie>()

    companion object {
        private const val EXTRA_TYPE = "type"
        private const val TYPE_DAILY = "daily_reminder"
        private const val TYPE_RELEASE = "release_reminder"
        private const val ID_DAILY_REMINDER = 10
        private const val ID_RELEASE_TODAY = 101
    }

    override fun onReceive(context: Context?, intent: Intent?) {

        val type = intent?.getStringExtra(EXTRA_TYPE)
        if (type == TYPE_DAILY) {
            if (context != null) {
                showDailyReminder(context)
            }
        } else if (type == TYPE_RELEASE) {
            if (context != null) {
                getReleaseToday(context)
            }
        }
    }


    private fun getReminderTime(type: String): Calendar {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, if (type == TYPE_DAILY) 7 else 8)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)

        if (calendar.before(Calendar.getInstance())) {
            calendar.add(Calendar.DATE, 1)
        }

        return calendar
    }

    private fun getReminderIntent(context: Context,type: String): Intent {
        val intent = Intent(context, DailyReminder::class.java)
        intent.putExtra(EXTRA_TYPE, type)
        return intent
    }

    fun setReleaseTodayReminder(context: Context) {
        val pendingIntent = PendingIntent.getBroadcast(context, ID_RELEASE_TODAY, getReminderIntent(context,TYPE_RELEASE), 0)

        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, getReminderTime(TYPE_RELEASE).timeInMillis, AlarmManager.INTERVAL_DAY, pendingIntent)

    }

    fun setDailyReminder(context: Context) {
        val pendingIntent = PendingIntent.getBroadcast(context, ID_DAILY_REMINDER, getReminderIntent(context,TYPE_DAILY), 0)

        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, getReminderTime(TYPE_DAILY).timeInMillis, AlarmManager.INTERVAL_DAY, pendingIntent)

    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun getReleaseToday(context: Context){
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val date = Date()
        val now = dateFormat.format(date)

        GlobalScope.launch {
            try {
                val movie = now.let { utilsApi.getReleaseToday(it) }
                mMovie.clear()
                movie?.let { mMovie.addAll(it) }

                for (i in mMovie){
                    var id = 5
                    val title = i.title
                    val content = "$title has been released today"
                    showReleaseToday(context,i.title,content, id)
                     ++id


                    println(i.title)
                }

            }catch (e : Throwable){
                e.printStackTrace()
            }

        }
    }





    private fun showReleaseToday(context: Context, title: String, desc: String, id: Int) {
        val CHANNEL_ID = "Channel_2"
        val CHANNEL_NAME = "Today release channel"

        val intent = Intent(context, AccountFragment::class.java)
        val pendingIntent = PendingIntent.getActivity(context, id, intent,
            PendingIntent.FLAG_UPDATE_CURRENT)

        val uriRingtone = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val mNotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val mBuilder = NotificationCompat.Builder(context, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_clapperboard)
            .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.drawable.ic_clapperboard))
            .setContentTitle(title)
            .setContentText(desc)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .setSound(uriRingtone)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        val notification = mBuilder.build()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            val channel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT)
            mBuilder.setChannelId(CHANNEL_ID)

            mNotificationManager.createNotificationChannel(channel)
        }
        mNotificationManager.notify(id, notification)
    }

    private fun showDailyReminder(context: Context) {
        val NOTIFICATION_ID = 1
        val CHANNEL_ID = "Channel_1"
        val CHANNEL_NAME = "Daily Reminder channel"

        val intent = Intent(context, AccountFragment::class.java)
        val pendingIntent = PendingIntent.getActivity(context, NOTIFICATION_ID, intent,
            PendingIntent.FLAG_UPDATE_CURRENT)

        val uriRingtone = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val mNotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val mBuilder = NotificationCompat.Builder(context, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_clapperboard)
            .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.drawable.ic_clapperboard))
            .setContentTitle(context.resources.getString(R.string.app_name))
            .setContentText(context.resources.getString(R.string.daily_message_reminder))
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .setSound(uriRingtone)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)

        val notification = mBuilder.build()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            val channel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT)
            mBuilder.setChannelId(CHANNEL_ID)

            mNotificationManager.createNotificationChannel(channel)
        }
        mNotificationManager.notify(NOTIFICATION_ID, notification)
    }

    private fun cancelReminder(context: Context, type: String) {
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, DailyReminder::class.java)
        val requestCode = if (type.equals(TYPE_DAILY, ignoreCase = true)) ID_DAILY_REMINDER else ID_RELEASE_TODAY
        val pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0)
        pendingIntent.cancel()
        alarmManager.cancel(pendingIntent)
    }

    fun cancelDailyReminder(context: Context) {
        cancelReminder(context, TYPE_DAILY)
    }

    fun cancelReleaseToday(context: Context) {
        cancelReminder(context, TYPE_RELEASE)
    }

}
