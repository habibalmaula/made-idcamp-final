package com.habibalmaula.submissionmadeidcamp.reminder

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.habibalmaula.submissionmadeidcamp.R
import com.habibalmaula.submissionmadeidcamp.data.entity.Movie
import com.habibalmaula.submissionmadeidcamp.data.network.UtilsApi
import kotlinx.coroutines.coroutineScope

class ReminderWorker (context: Context, params: WorkerParameters) : CoroutineWorker(context, params){

    private val utilsApi = UtilsApi()
    private val mMovie = ArrayList<Movie>()


    companion object {
        private val TAG = ReminderWorker::class.java.simpleName
        const val EXTRA_DATE = "date"
        var NOTIFICATION_ID = 1
        const val CHANNEL_ID = "channel_01"
        const val CHANNEL_NAME = "habib_channel"
    }
    @RequiresApi(Build.VERSION_CODES.O)
    override suspend fun doWork(): Result  = coroutineScope{
        val formatted = inputData.getString(EXTRA_DATE)

        try {
            val movie = formatted?.let { utilsApi.getReleaseToday(it) }
            mMovie.clear()
            movie?.let { mMovie.addAll(it) }

            for (i in mMovie){
                showNotification(i.title)


                println(i.title)
            }

        }catch (e : Throwable){
            e.printStackTrace()
        }


        Result.success()
    }


    private fun showNotification(title: String) {
        val notificationManager = applicationContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        val notification: NotificationCompat.Builder = NotificationCompat.Builder(applicationContext, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_clapperboard)
            .setContentTitle(title)
            .setContentText("Was Release Today!")
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setDefaults(NotificationCompat.DEFAULT_ALL)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH)
            notification.setChannelId(CHANNEL_ID)
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(++NOTIFICATION_ID, notification.build())
    }


}